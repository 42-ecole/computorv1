import sys
import re
from element import Element

elements = []
regex = '[\*,\+,\-\/]? ?\d{0,100}\.?\d{0,100} ?[\*,\+,\-\/]? ?[x,X]\^\d?'

def parseInput(s):
	s = s.replace(' ', '')
	res = re.finditer(regex, s)
	for match in res:
		elements.append(Element(match.group(), match.start()))
	return s
	

def moveLeftBehindEqual(elements, s):
	equalPos = int(s.find('='))
	if (equalPos == -1):
		print("There is no equalation in the input!")
		exit()
	for el in elements:
		if (el.pos > equalPos):
			el.changeDash()


def printElements(elements):
	for el in elements:
		print(el.data, el.k, el.degree)

def sign(x): 
	if (1-(x<=0) == 0):
		return ('-')
	else:
		return ('+')

def printOperator(operator):
	sys.stdout.write(' ')
	sys.stdout.write(operator)
	sys.stdout.write(' ')

def printEquation(elements):
	for el in elements:
		operators = re.findall('[\*,\+,\-\/]', el.data)
		printOperator(sign(el.k))
		if (el.k.is_integer()):
			sys.stdout.write(str(int(abs(el.k))))
		else:
			sys.stdout.write(str(abs(el.k)))
		if (el.degree > 1):
			if (len(operators) == 1):
				printOperator(operators[0])
			else:
				printOperator(operators[1])
			sys.stdout.write('X^' + str(el.degree))
		elif (el.degree == 1):
			if (len(operators) == 1):
				printOperator(operators[0])
			else:
				printOperator(operators[1])
			sys.stdout.write('X')
	sys.stdout.write(" = 0\n")

def sumElements(elements):
	for i in range(len(elements)):
		for j in range(i + 1, len(elements)):
			if (elements[i].degree == elements[j].degree):
				elements[i].k += elements[j].k
				del elements[j]
				break
	
def computeDiscriminant(elements):
	d = -1
	if (len(elements) == 3):
		d = elements[1].k ** 2 - 4 * elements[0].k * elements[2].k
	elif (elements[1].degree == 1):
		d = elements[1].k ** 2
	return d

def printDiscriminant(d):
	if (d > 0):
		print("Discriminant is strictly positive, the two solutions are: ")
	elif (d == 0):
		print("The solution is: ")
	else:
		print("Discriminant is strictly negative, the two complex solutions are: ")

def computeAndPrintRoots(elements, d: float):
	if (d != 0):
		print((-1.0 * elements[1].k + d ** 0.5) / (2.0 * elements[0].k))
		print((-1.0 * elements[1].k - d ** 0.5) / (2.0 * elements[0].k))
	elif (d == 0):
		print((-1 * elements[1].k) / (2 * elements[0].k))

def printPolinomOneDegree(elements):
	print("The solution is: " + str(-1 * elements[1].k / elements[0].k))

def printIncompleteQuadratic(elements):
	print("Two solutions are: ")
	roots = (-1 * elements[1].k / elements[0].k) ** 0.5
	print(roots)
	print(-1 * roots)

def printIncompleteQuadraticA(elements):
	print("The solution is: ")
	print(0)


if (len(sys.argv) == 2):
	inputstr = parseInput(sys.argv[1])
	moveLeftBehindEqual(elements, inputstr)
	sumElements(elements)
	elements.sort(key=lambda el: el.degree, reverse=True)
	if (len(elements) == 0):
		print("Wrong input.")
		exit()
	sys.stdout.write("Reduced form: ")
	printEquation(elements)
	degree = elements[0].degree
	print("Polynomal degree: " + str(degree))
	if (degree == 2):
		if (len(elements) == 1):
			printIncompleteQuadraticA(elements)
		elif (len(elements) == 3 or elements[1].degree == 1):
			d = computeDiscriminant(elements)
			printDiscriminant(d)
			computeAndPrintRoots(elements, d)
		elif (len(elements) == 2):
			printIncompleteQuadratic(elements)

	elif (degree == 1):
		printPolinomOneDegree(elements)
	elif (degree == 0 and len(elements) == 1):
		if (elements[0].k == 0):
			print("Every solution.")
		else:
			print("No solution.")
	else:
		print("The polynomial degree is strictly greater than 2, I can't solve.")
