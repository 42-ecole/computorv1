import re

class Element:

	def __init__(self, data, pos):
		self.data = data
		self.pos = int(pos)
		res = re.search('[x,X]\^\d', data)
		if (res == None):
			print("Wrong input.")
			exit()
		self.degree = int(res.group(0)[-1:])
		self.k = float(re.search('^([\*,\+,\-\/]?\d{0,100}\.?\d{0,100})', data).group(0))

	def changeDash(self):
		if (self.data[0] == '+'):
			self.data = self.data[:0] + '-' + self.data[1:]
			self.k *= -1
		elif (self.data[0].isdigit()):
			self.data = "-" + self.data
			self.k *= -1
		elif (self.data[0] == '-'):
			self.data = self.data[:0] + '+' + self.data[1:]